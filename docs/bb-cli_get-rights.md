## bb-cli get-rights

Gets baking and endorsing rights.

### Synopsis

Gets baking and endorsing rights of BB instance.

```
bb-cli get-rights [flags]
```

### Options

```
  -f, --future-blocks int   Sets how many future blocks should be probed for rights. (default 60)
  -h, --help                help for get-rights
```

### Options inherited from parent commands

```
  -l, --log-level string       Sets output log format (json/text/auto) (default "info")
  -o, --output-format string   Sets output log format (json/text/auto) (default "auto")
  -p, --path string            Path to bake buddy instance (default "/bake-buddy")
```

### SEE ALSO

* [bb-cli](bb-cli.md)	 - Bake Buddy CLI

###### Auto generated by spf13/cobra on 29-Aug-2022
